import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { GlobalStyle } from "./globalStyle";
import {StyledCenterContent} from './components/centerComponent';
import App from "./app";



ReactDOM.render(
  <Fragment>
    <GlobalStyle />
    <StyledCenterContent>
      <App/>
    </StyledCenterContent>
  </Fragment>,
  document.getElementById("root")
);
