/* eslint-disable react/react-in-jsx-scope */
import React , { Fragment, useState, ReactNode } from "react"
import PhoneNumberInput from "./components/phoneNumberInput/phoneNumberInput";
import Toast from "./components/notification/toast";
import Card from "./components/card/card";
import SelectContact from "./components/selectContact/selectContact";

const App = () => {
    const [notifcation, setNotifcation] = useState<ReactNode>();
    const [contact, setContact] = useState<string>();

    const supported = ('contacts' in navigator && 'ContactsManager' in window);

    const handlePhoneInputClick = () => {
        const contacts = (navigator as any).contacts;
        if(contacts){
            (navigator as any).contacts.select( ['tel', 'name'], { multiple: false }).then((contacts: any[]) => {
                
                contacts.forEach(contact => {
                    setNotifcation(<Card><div>{contact.name} - {contact.tel}</div></Card>)
                    setContact(contact.tel[0]);
                });
            }).catch((ex: any) => {
                setNotifcation(<Card><div>issue with contacts : {ex.toString()}</div></Card>)
                return;
            });
        }
    }

    return (
        <Fragment>
            <PhoneNumberInput contactNumber={contact} />
            <Toast notification={notifcation}/>
            {
                supported &&  <SelectContact onClick={handlePhoneInputClick}/>
            }
            {
                !supported && <Toast notification={<Card>
                    <div>Browser or device not supported</div>
                    <div>Try with chrome 80 on an android device with at least android M or later</div>
                    </Card>}/>
            }
        </Fragment>
    );
}

export default App;