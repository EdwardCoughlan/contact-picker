import styled from "styled-components";


export const StyledCenterContent = styled.div<{}>`
    height: ${window.innerHeight}px;
    display : flex;
    flex-direction: column;
    flex-wrap: nowrap;
    justify-content: space-around;
    > * {
        align-self: center;
    }
`;