import React, {  ReactNode } from 'react';
import styled from 'styled-components';

type Props ={
    children: ReactNode
}
const Card = (props: Props) => {
    const {children} = props;
    return <StyledCard>{children}</StyledCard>
}
export default Card;
const StyledCard = styled.div`
    border-radius: 4px;
    background: #3c4042;
    height: max-content;
    padding: 10px 14px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    box-shadow: 5px 5px 32px 8px rgba(0,0,0,0.13);  
    > * {
        align-self: center;
        max-width: 80vw;
        color: white;
        text-align: left;
    }
`;