import React, { useState, useRef, useEffect } from 'react';
import styled, { css } from 'styled-components';


type Props = {
    contactNumber ? :  string;
}
const PhoneNumberInput = (props : Props) => {
    const {contactNumber} = props;
    const [focused, setFocused] = useState(false);
    const input = useRef<HTMLInputElement>(null);
    const hasInputValue = () : boolean => {
        return (input !== null && input.current !== null && input.current.value.length > 0) || (contactNumber !== undefined && contactNumber !== null && contactNumber.length > 0) ;
    }
    useEffect(() => {
        if(input.current){
            input.current.value = contactNumber ? contactNumber : ''
        }
    }, [contactNumber, input])

    return <StyledInputWrapper>
        <StyledLabel focused={focused} inputHasValue={hasInputValue()}>Enter phone number</StyledLabel>
            
        <StyledInput ref={input} 
            type="tel"  
            onFocus={() => setFocused(true)}  
            onBlur={() => setFocused(false)} 
            maxLength={15}
        />
    </StyledInputWrapper>
}

export default PhoneNumberInput;

const StyledInputWrapper = styled.div`
    position: relative;
    width: 90%
`;

const StyledInput = styled.input`
outline: none;
width: 100%;
font-size: 16px;
border-style: none none solid;
border-width: 1px;
padding: 0.95em 0px 0.4em;
border-color: white;
background: transparent none repeat scroll 0% 0%;
color: white;
`;

const StyledLabel = styled.label<{focused: boolean, inputHasValue: boolean}>`
    position: absolute;
    transition: top 0.2s cubic-bezier(0.175, 0.885, 0.425, 1.485) 0s;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    max-width: 100%;
    top: 15px;
    font-size: 16px;
    color: white;
    pointer-events: none;

    ${p => (p.focused || p.inputHasValue )&& css`
        top: 0px;
        font-size: 10px;
    `}
`;