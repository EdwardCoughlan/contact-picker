import React, { ReactNode} from 'react';
import styled from 'styled-components';


type Props = {
    notification : ReactNode
}

export enum NotificationType {
    positive = 'positive',
    negative = 'negative',
    warn ='warn'
}

const Toast = (props: Props) => {
    let {notification} = props;
    
    return <StyledToast >{notification}</StyledToast>;
}

const StyledToast = styled.div`
    position: fixed;
    bottom: 24px;
    height: max-content;
`

export default Toast;