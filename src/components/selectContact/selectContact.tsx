import React, { useState } from 'react';
import styled, { css } from 'styled-components';
type Props = {
    onClick : () => void;
}

const SelectContact = (props : Props) => {
    const {onClick} = props;
    const [touched, setTouched] = useState(false);


    return( 
        <StyledSelectContact 
            onTouchStart={() => {
                setTouched(true)
            }} 
            onTouchEnd={() => {
                    onClick();
                    setTouched(false);
            }}
            touched={touched}
        >
            <StyledLine />
            <StyledLine />
        </StyledSelectContact>
    )
}

const StyledLine = styled.div`
    position: absolute;
    height : 3px;
    width: 75%;
    top: 30px;
    left: 7.5px;
    border-radius: 1px;
    transform-origin: center; 
    background-color: black;
    cursor: unset;
`;

const StyledSelectContact = styled.div<{touched : boolean}>`
    position: fixed;
    bottom:20px;
    right: 20px;
    height: 60px;
    width: 60px;
    background-color: white;
    border-radius: 50%;
    > *:first-child{
        transform: rotate(90deg);
    }
    transition: all 300ms cubic-bezier(.95,.03,.72,.18);
    ${(p) => p.touched && css`
        transform: rotate(75deg);
    `}
`

export default SelectContact;